package heuristic;

import struct.Celda;

/**
 * Heurística cuyo valor depende de la suma de filas y columnas hasta la salida
 */
public class Manhattan implements Heuristic {

    @Override
    public double estimation(Celda actual, Celda salida) {
        int filaActual, colActual, filaSalida, colSalida;
        filaActual = actual.getId() / 10;
        filaSalida = salida.getId() / 10;
        colActual = actual.getId() % 10;
        colSalida = salida.getId() % 10;

        return (Math.abs(filaSalida - filaActual) + Math.abs(colSalida - colActual));
    }

    @Override
    public String getName() {
        return Manhattan.class.getSimpleName();
    }
}
