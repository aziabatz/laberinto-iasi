package heuristic;

import struct.Celda;

/**
 * Heurística que tiene en cuenta el id de la celda
 */
public class Tablero implements Heuristic {
    @Override
    public double estimation(Celda actual, Celda salida) {
        return Math.abs(actual.getId() - salida.getId());
    }

    @Override
    public String getName() {
        return Tablero.class.getSimpleName();
    }
}
