package heuristic;

import struct.Celda;

/**
 * Heurística que mide la distancia directa(euclídea)
 */
public class Euclediana implements Heuristic {
    @Override
    public double estimation(Celda actual, Celda salida) {
        int distanciaFila = Math.abs(actual.getId() / 10 - salida.getId() / 10);
        int distanciaCol = Math.abs(actual.getId() % 10 - salida.getId() % 10);
        return Math.sqrt(
                Math.pow(distanciaFila, 2)
                        +
                        Math.pow(distanciaCol, 2)
        );

    }


    @Override
    public String getName() {
        return "Euclediana";
    }
}
