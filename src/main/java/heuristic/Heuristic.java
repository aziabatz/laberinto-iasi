package heuristic;

import com.sun.istack.internal.NotNull;
import struct.Celda;

public interface Heuristic {
    /**
     * Devuelve la heurística de una determinada celda con respecto a la salida
     *
     * @return Valor de la heurística
     */
    double estimation(@NotNull Celda actual, @NotNull Celda salida);//Heuristic estimation. H'

    /**
     * Devuelve nombre de la heurística
     */
    String getName();
}
