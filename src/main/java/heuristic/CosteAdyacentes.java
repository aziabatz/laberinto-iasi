package heuristic;

import struct.Celda;

/**
 * Heurística que tiene en cuenta los costes de las celdas adyacentes
 */
public class CosteAdyacentes implements Heuristic {
    @Override
    public double estimation(Celda actual, Celda salida) {
        Celda[] ady = {actual.getOESTE(), actual.getESTE(), actual.getNORTE(), actual.getSUR()};
        int costes = 0;
        for (Celda c :
                ady) {
            if (c != null)
                costes += c.getCoste();
            else
                costes += 5;
        }
        return costes;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }
}
