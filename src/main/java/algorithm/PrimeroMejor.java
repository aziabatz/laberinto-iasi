package algorithm;

import struct.Celda;
import struct.Laberinto;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Algoritmo A*
 */
public class PrimeroMejor extends Algorithm {
    public PrimeroMejor() {
        NAME = "Primero Mejor ";
    }
    /**
     * Lista de nodos abiertos
     */
    List<CommonNodo> abiertos;
    /**
     * Lista de nodos cerrados
     */
    List<CommonNodo> cerrados;
    /**
     * Lista de nodos solución
     */
    List<CommonNodo> solucionList;

    public void setup(Laberinto laberinto) {
        super.setup(laberinto);
        abiertos = new LinkedList<>();
        cerrados = new LinkedList<>();
        solucionList = new LinkedList<>();
    }

    public void runAlgor() {
        /**
         *Crear abiertos
         * evaluar los abiertos y cerrar los invalidos
         *
         */
        abiertos.add(new CommonNodo(actual, heuristic.estimation(actual, laberinto.getSalida()), null));

        double hEstimation;
        double gCost;


        int i = 0;
        while (costeHastaAhora <= movimientos.length && !solucion && !abiertos.isEmpty()) {
            CommonNodo nodoActual = abiertos.remove(0);
            actual = nodoActual.getCelda();
            solucion = comprobarSolucion(actual);
            ponerAbiertos(nodoActual);
            cerrados.add(nodoActual);
            Comparator<CommonNodo> cmp = Comparator.comparingDouble(nodo -> nodo.getHeuristica());
            abiertos.sort(cmp);

        }

        if (!solucion && costeHastaAhora > laberinto.getUmbral())
            causaFallo = "Umbral sobrepasado";

        CommonNodo nFinal = cerrados.get(cerrados.size() - 1);
        LinkedList<CommonNodo> linkedList = new LinkedList<>();
        costeHastaAhora = 0;

        int movs = cerrados.size() - 1;
        while (nFinal != null) {
            linkedList.addFirst(nFinal);
            costeHastaAhora += nFinal.getCelda().getCoste();
            nFinal = nFinal.getNodoPadre();
            movsHastaAhora++;
        }
        solucionList = linkedList;
        movimientos = new MOVS[linkedList.size()];
        for (int j = 0; j < movimientos.length; j++) {
            if (j + 1 < movimientos.length)
                movimientos[j] = getMOV(linkedList.get(j + 1), linkedList.get(j));
        }

        nodosGenerados.addAll(cerrados);
        nodosGenerados.addAll(abiertos);
    }


    /**
     * Añade los nodos no explorados a abiertos
     *
     * @param nodoActual CommonNodo actual
     */
    private void ponerAbiertos(CommonNodo nodoActual) {
        Celda norte, este, oeste, sur;
        Celda noreste, noroeste, sureste, suroeste;
        Celda salida = laberinto.getSalida();
        int coste = costeHastaAhora + actual.getCoste();
        Celda actual = nodoActual.getCelda();
        norte = actual.getNORTE();
        noreste = actual.getNORESTE();
        noroeste = actual.getNOROESTE();
        este = actual.getESTE();
        oeste = actual.getOESTE();
        sur = actual.getSUR();
        sureste = actual.getSURESTE();
        suroeste = actual.getSUROESTE();

        if (norte != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(norte)))
            abiertos.add(new CommonNodo(norte, heuristic.estimation(norte, laberinto.getSalida()), nodoActual));
        if (noreste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(noreste)))
            abiertos.add(new CommonNodo(noreste, heuristic.estimation(noreste, laberinto.getSalida()), nodoActual));
        if (noroeste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(noroeste)))
            abiertos.add(new CommonNodo(noroeste, heuristic.estimation(noroeste, laberinto.getSalida()), nodoActual));
        if (este != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(este)))
            abiertos.add(new CommonNodo(este, heuristic.estimation(este, laberinto.getSalida()), nodoActual));
        if (oeste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(oeste)))
            abiertos.add(new CommonNodo(oeste, heuristic.estimation(oeste, laberinto.getSalida()), nodoActual));
        if (sur != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(sur)))
            abiertos.add(new CommonNodo(sur, heuristic.estimation(sur, laberinto.getSalida()), nodoActual));
        if (sureste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(sureste)))
            abiertos.add(new CommonNodo(sureste, heuristic.estimation(sureste, laberinto.getSalida()), nodoActual));
        if (suroeste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(suroeste)))
            abiertos.add(new CommonNodo(suroeste, heuristic.estimation(suroeste, laberinto.getSalida()), nodoActual));
    }

    public String execOutput() {
        String resultado = new String();
        resultado += "Primero Mejor " + heuristic.getName() + "\n";
        resultado += solucion + "\n";
        for (int i = 0; i < movimientos.length && movimientos[i] != null; i++) {
            resultado += movimientos[i].toString() + " ";
        }

        resultado += "\n" + laberinto.colouredLaberint(
                (solucionList.parallelStream().map(CommonNodo::getCelda).collect(Collectors.toSet())),
                (abiertos.parallelStream().map(CommonNodo::getCelda).collect(Collectors.toSet())),
                (cerrados.parallelStream().map(CommonNodo::getCelda).collect(Collectors.toSet()))
        );

        resultado += "Coste: " + costeHastaAhora + "\n";
        resultado += "Movimientos: " + movsHastaAhora + "\n";
        resultado += "Nodos generados en memoria: " + nodosSize() + "\n";
        //resultado+="\n";

        return resultado;
    }

}
