package algorithm;

import struct.Celda;

import java.util.Comparator;

/**
 * Clase que representa los nodos y estados de resolución
 */
public class CommonNodo implements Comparator<CommonNodo> {
    private CommonNodo nodoPadre;
    private Celda celda;
    private double heuristica;

    /**
     * Constructor de la clase
     *
     * @param celda      Celda actual del nodo
     * @param heuristica Heurística de la celda
     * @param padre      Nodo padre(procedencia)
     */
    public CommonNodo(Celda celda, double heuristica, CommonNodo padre) {
        this.nodoPadre = padre;
        this.celda = celda;
        this.heuristica = heuristica;
    }

    public double getF() {
        return getHeuristica();
    }

    public CommonNodo getNodoPadre() {
        return nodoPadre;
    }

    public void setNodoPadre(CommonNodo nodoPadre) {
        this.nodoPadre = nodoPadre;
    }

    public Celda getCelda() {
        return celda;
    }

    public void setCelda(Celda celda) {
        this.celda = celda;
    }

    public double getHeuristica() {
        return heuristica;
    }

    public void setHeuristica(double heuristica) {
        this.heuristica = heuristica;
    }

    @Override
    public int compare(CommonNodo nodo, CommonNodo t1) {
        return (int)
                (

                        (1000 * nodo.getF())
                                -
                                (1000 * t1.getF())

                );
    }

    @Override
    public int hashCode() {
        return celda.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        return celda.equals(((CommonNodo) obj).getCelda());
    }

    @Override
    public String toString() {
        return "CommonNodo{\n\t" + celda.toString() + "}";
    }
}
