package algorithm;

import heuristic.Heuristic;
import struct.Celda;
import struct.Laberinto;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Clase algoritmo
 */
public abstract class Algorithm {
    /**
     * Nombre del algoritmo
     */
    protected String NAME;
    /**
     * Laberinto sobre el cual va a trabajar el algoritmo
     */
    protected Laberinto laberinto;
    /**
     * Array de movimientos que se han efectuado
     */
    protected MOVS[] movimientos;
    /**
     * Celda del estado actual de resolución
     */
    protected Celda actual;
    /**
     * Bandera que indica si se ha llegado a la solución
     */
    protected boolean solucion;
    /**
     * Coste acumulado que lleva el algoritmo hasta ahora
     */
    protected int costeHastaAhora;
    /**
     * Número de movimientos efectuados
     */
    protected int movsHastaAhora;
    /**
     * Heuristica usada
     */
    protected Heuristic heuristic;
    /**
     * Celdas por las que pasa el algoritmo
     */
    protected Set<Celda> celdas;
    /**
     * Lista de nodos generados
     */
    protected List<CommonNodo> nodosGenerados;
    /**
     * Causa de fallo del algoritmo
     */
    protected String causaFallo;

    /**
     * Método que prepara el algoritmo para ser ejecutado
     *
     * @param laberinto Laberinto en el que buscar la solución
     */
    public void setup(Laberinto laberinto) {
        this.laberinto = laberinto;
        movimientos = new MOVS[laberinto.getUmbral()];
        actual = laberinto.getLaberinto()[0][0];
        solucion = false;
        costeHastaAhora = 0;
        movsHastaAhora = 0;
        celdas = new HashSet<>();
        nodosGenerados = new LinkedList<>();
    }

    /**
     * Tiempo que ha empleado el algoritmo
     */
    private long time;

    /**
     * Evalúa la heurśitica de dos celdas
     *
     * @param anterior  Celda anterior
     * @param siguiente Siguiente celda
     * @return Devuelve true si la segunda celda tiene mejor heurística
     */
    public boolean heuristicaMejor(Celda anterior, Celda siguiente) {
        return !(heuristic.estimation(anterior, laberinto.getSalida()) <
                heuristic.estimation(siguiente, laberinto.getSalida()));
    }

    /**
     * Método que ejecuta el algoritmo
     */
    public abstract void runAlgor();

    /**
     * Método que comprueba si el estado actual es el estado solución
     *
     * @param actual Celda actual del estado de resolución;
     * @return Devuelve si se ha llegado a la solución
     */
    protected boolean comprobarSolucion(Celda actual) {
        return actual.equals(laberinto.getSalida());
    }

    /**
     * Indica la heurística a usar por el algoritmo
     * @param heuristic Referencia a la heurística que se debe usar
     */
    public void runWithHeuristic(Heuristic heuristic) {
        this.heuristic = heuristic;
    }

    /**
     * Devuelve el último estado del laberinto<br>
     *     Si se llegó a la solución<br>
     *     La causa del fallo en caso de no encontrar la solución<br>
     *     Los movimientos efectuados y su número<br>
     *     El coste acumulado<br>
     *     El tablero y el camino seguido
     * @return
     */
    public String execOutput() {
        String resultado = new String();
        resultado += NAME + heuristic.getName() + "\n";
        resultado += solucion + "\n";
        if (!solucion)
            resultado += causaFallo + " \n";
        for (int i = 0; i < movimientos.length && movimientos[i] != null; i++) {
            resultado += movimientos[i].toString() + " ";
        }

        resultado += "\n" + laberinto.colouredLaberint(celdas);

        resultado += "Coste: " + costeHastaAhora + "\n";
        resultado += "Movimientos: " + movsHastaAhora + "\n";
        resultado += "Nodos generados en memoria: " + nodosSize() + "\n";
        //resultado+="\n";

        return resultado;
    }

    public String getNAME() {
        return NAME;
    }

    public Laberinto getLaberinto() {
        return laberinto;
    }

    public Celda getActual() {
        return actual;
    }

    public boolean isSolucion() {
        return solucion;
    }

    public int getCosteHastaAhora() {
        return costeHastaAhora;
    }

    public int getMovsHastaAhora() {
        return movsHastaAhora;
    }

    public Heuristic getHeuristic() {
        return heuristic;
    }

    public Set<Celda> getCeldas() {
        return celdas;
    }

    public List<CommonNodo> getNodosGenerados() {
        return nodosGenerados;
    }

    public String getCausaFallo() {
        return causaFallo;
    }

    public int nodosSize() {
        return new HashSet<CommonNodo>(nodosGenerados).size();
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    /**
     * Calcula los movimientos realizados por el algoritmo
     * @return Devuelve un array de movimientos
     */
    public MOVS[] getMovimientos() {
        List<MOVS> movs = new LinkedList<>();
        CommonNodo nodo = ((LinkedList<CommonNodo>) nodosGenerados).getLast();
        while (nodo.getNodoPadre() != null) {
            movs.add(getMOV(nodo, nodo.getNodoPadre()));
            nodo = nodo.getNodoPadre();
        }
        movimientos = new MOVS[movs.size()];
        for (int i = 0; i < movimientos.length; i++) {
            movimientos[i] = movs.get(-i - 1 + movs.size());
        }
        return movimientos;
    }

    /**
     * Calcula el movimiento según la procedencia
     * @param nodo Nodo
     * @param padre Su nodo padre
     * @return Devuelve el movimiento, null si no hay procedencia del nodo padre
     */
    protected MOVS getMOV(CommonNodo nodo, CommonNodo padre) {
        Celda celda = nodo.getCelda();
        Celda celdaPrev = padre.getCelda();
        if (celda.getNORTE() != null && celda.getNORTE().equals(celdaPrev))
            return MOVS.SUR;
        if (celda.getESTE() != null && celda.getESTE().equals(celdaPrev))
            return MOVS.OESTE;
        if (celda.getOESTE() != null && celda.getOESTE().equals(celdaPrev))
            return MOVS.ESTE;
        if (celda.getSUR() != null && celda.getSUR().equals(celdaPrev))
            return MOVS.NORTE;
        if (celda.getNORESTE() != null && celda.getNORESTE().equals(celdaPrev))
            return MOVS.SUROESTE;
        if (celda.getNOROESTE() != null && celda.getNOROESTE().equals(celdaPrev))
            return MOVS.SURESTE;
        if (celda.getSURESTE() != null && celda.getSURESTE().equals(celdaPrev))
            return MOVS.NOROESTE;
        if (celda.getSUROESTE() != null && celda.getSUROESTE().equals(celdaPrev))
            return MOVS.NORESTE;
        return null;
    }

    /**
     * Movimientos posibles dentro del laberinto
     */
    enum MOVS {
        NOROESTE, NORTE, NORESTE,
        OESTE, ESTE,
        SUROESTE, SUR, SURESTE
    }

}
