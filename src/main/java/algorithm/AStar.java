package algorithm;

import struct.Celda;
import struct.Laberinto;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Algoritmo A*
 */
public class AStar extends Algorithm {
    public AStar() {
        NAME = "A* ";
    }
    /**
     * Lista de nodos abiertos
     */
    List<Nodo> abiertos;
    /** Lista de nodos cerrados*/
    List<Nodo> cerrados;
    /** Lista de nodos solución*/
    List<Nodo> solucionList;

    public void setup(Laberinto laberinto) {
        super.setup(laberinto);
        abiertos = new LinkedList<>();
        cerrados = new LinkedList<>();
        solucionList = new LinkedList<>();
    }

    public void runAlgor() {
        /**
         *Crear abiertos
         * evaluar los abiertos y cerrar los invalidos
         *
         */
//        for (int i = 0; i < 10; i++) {
//            for (int j = 0; j < 10; j++) {
//                abiertos.add(laberinto.getLaberinto()[i][j]);
//            }
//        }
        abiertos.add(new Nodo(actual, costeHastaAhora, heuristic.estimation(actual, laberinto.getSalida()), null));

        double hEstimation;
        double gCost;


        int i = 0;
        while (costeHastaAhora <= laberinto.getUmbral() && !solucion && !abiertos.isEmpty()) {
            Nodo nodoActual = abiertos.remove(0);
            actual = nodoActual.getCelda();
            solucion = comprobarSolucion(actual);

            ponerAbiertos(nodoActual);
            cerrados.add(nodoActual);
            Comparator<Nodo> cmp = Comparator.comparingDouble(nodo -> nodo.getF());
            abiertos.sort(cmp);


            costeHastaAhora = nodoActual.getG();

        }

        if (!solucion && costeHastaAhora > laberinto.getUmbral())
            causaFallo = "Umbral sobrepasado";

        Nodo nFinal = cerrados.get(cerrados.size() - 1);
        LinkedList<Nodo> linkedList = new LinkedList<>();


        int movs = cerrados.size() - 1;
        while (nFinal != null) {
            linkedList.addFirst(nFinal);

            nFinal = (Nodo) nFinal.getNodoPadre();
            movsHastaAhora++;
        }
        solucionList = linkedList;
        movimientos = new MOVS[linkedList.size()];
        for (int j = 0; j < movimientos.length; j++) {
            if (j + 1 < movimientos.length)
                movimientos[j] = getMOV(linkedList.get(j + 1), linkedList.get(j));
        }

        nodosGenerados.addAll(abiertos);
        nodosGenerados.addAll(cerrados);
    }

    /**
     * Añade los nodos no explorados a abiertos
     * @param nodoActual CommonNodo actual
     */
    private void ponerAbiertos(Nodo nodoActual) {
        Celda norte, este, oeste, sur;
        Celda noreste, noroeste, sureste, suroeste;
        Celda salida = laberinto.getSalida();
        int coste = nodoActual.getG() + actual.getCoste();
        Celda actual = nodoActual.getCelda();
        norte = actual.getNORTE();
        noreste = actual.getNORESTE();
        noroeste = actual.getNOROESTE();
        este = actual.getESTE();
        oeste = actual.getOESTE();
        sur = actual.getSUR();
        sureste = actual.getSURESTE();
        suroeste = actual.getSUROESTE();

        if (norte != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(norte)))
            abiertos.add(new Nodo(norte, coste, heuristic.estimation(norte, laberinto.getSalida()), nodoActual));
        if (noreste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(noreste)))
            abiertos.add(new Nodo(noreste, coste, heuristic.estimation(noreste, laberinto.getSalida()), nodoActual));
        if (noroeste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(noroeste)))
            abiertos.add(new Nodo(noroeste, coste, heuristic.estimation(noroeste, laberinto.getSalida()), nodoActual));
        if (este != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(este)))
            abiertos.add(new Nodo(este, coste, heuristic.estimation(este, laberinto.getSalida()), nodoActual));
        if (oeste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(oeste)))
            abiertos.add(new Nodo(oeste, coste, heuristic.estimation(oeste, laberinto.getSalida()), nodoActual));
        if (sur != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(sur)))
            abiertos.add(new Nodo(sur, coste, heuristic.estimation(sur, laberinto.getSalida()), nodoActual));
        if (sureste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(sureste)))
            abiertos.add(new Nodo(sureste, coste, heuristic.estimation(sureste, laberinto.getSalida()), nodoActual));
        if (suroeste != null && cerrados.parallelStream().noneMatch(nodo -> nodo.getCelda().equals(suroeste)))
            abiertos.add(new Nodo(suroeste, coste, heuristic.estimation(suroeste, laberinto.getSalida()), nodoActual));
    }

    public String execOutput() {
        String resultado = new String();
        resultado += "A* " + heuristic.getName() + "\n";
        resultado += solucion + "\n";
        for (int i = 0; i < movimientos.length && movimientos[i] != null; i++) {
            resultado += movimientos[i].toString() + " ";
        }

        resultado += "\n" + laberinto.colouredLaberint(
                (solucionList.parallelStream().map(Nodo::getCelda).collect(Collectors.toSet())),
                (abiertos.parallelStream().map(Nodo::getCelda).collect(Collectors.toSet())),
                (cerrados.parallelStream().map(Nodo::getCelda).collect(Collectors.toSet()))
        );

        resultado += "Coste: " + costeHastaAhora + "\n";
        resultado += "Movimientos: " + movsHastaAhora + "\n";
        resultado += "Nodos generados en memoria: " + nodosSize() + "\n";
        //resultado+="\n";

        return resultado;
    }

    /**
     * Clase nodo para facilitar la parte dinámica del algoritmo
     */
    private class Nodo extends CommonNodo {
        private int g;

        public Nodo(Celda celda, int g, double heuristica, CommonNodo padre) {
            super(celda, heuristica, padre);
            this.g = g;
        }

        @Override
        public double getF() {
            return getHeuristica() + g;
        }

        public int getG() {
            return g;
        }

        public void setG(int g) {
            this.g = g;
        }
    }
}
