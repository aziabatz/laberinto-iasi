package algorithm;

import struct.Celda;

public class EscaladaSimple extends Algorithm {
    public EscaladaSimple() {
        NAME = "Escalada Simple ";
    }

    public void runAlgor() {
        boolean noMejor = false;
        CommonNodo anterior = null;
        CommonNodo nodo;
        while (costeHastaAhora <= laberinto.getUmbral() && !solucion) {
            double heuristica = heuristic.estimation(actual, laberinto.getSalida());
            costeHastaAhora += actual.getCoste();
            celdas.add(actual);
            nodo = new CommonNodo(actual, heuristica, anterior);
            nodosGenerados.add(nodo);
            solucion = comprobarSolucion(actual);

            Celda[] adyacentes = new Celda[]{
                    actual.getNOROESTE(), actual.getNORTE(), actual.getNORESTE(),
                    actual.getOESTE(), actual.getESTE(),
                    actual.getSUROESTE(), actual.getSUR(), actual.getSURESTE()
            };

            int i = 0;
            for (i = 0; i < adyacentes.length; i++) {
                Celda ady = adyacentes[i];
                if (ady != null &&
                        !celdas.contains(ady) &&
                        heuristicaMejor(actual, ady) &&
                        costeHastaAhora + ady.getCoste() <= laberinto.getUmbral()) {
                    anterior = nodo;
                    actual = ady;
                    movsHastaAhora++;
                    break;//todo nomejor

                }
            }
            if (i >= adyacentes.length) {
                noMejor = true;

            }


        }
        causaFallo = "";
        if (!solucion) {

            if (costeHastaAhora > laberinto.getUmbral())
                causaFallo += "Umbral sobrepasado";
            else if (noMejor)
                causaFallo += "No hay nodos mejores";
            else
                causaFallo = "FALLO DESCONOCIDO";
        }

        getMovimientos();
    }
}
