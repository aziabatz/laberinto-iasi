package algorithm;

import struct.Celda;

public class EscaladaMaxPendiente extends Algorithm {
    public EscaladaMaxPendiente() {
        NAME = "Escalada Máxima Pendiente ";
    }

    public void runAlgor() {
        boolean noMejor = false;
        CommonNodo anterior, nodo;
        anterior = null;
        nodo = null;
        while (!solucion && !noMejor && costeHastaAhora <= laberinto.getUmbral()) {
            double heuristica = heuristic.estimation(actual, laberinto.getSalida());
            costeHastaAhora += actual.getCoste();
            celdas.add(actual);
            nodo = new CommonNodo(actual, heuristica, anterior);
            nodosGenerados.add(nodo);
            solucion = comprobarSolucion(actual);

            Celda[] adyacentes = new Celda[]{
                    actual.getNOROESTE(), actual.getNORTE(), actual.getNORESTE(),
                    actual.getOESTE(), actual.getESTE(),
                    actual.getSUROESTE(), actual.getSUR(), actual.getSURESTE()
            };
            double hActual, hAnterior = heuristic.estimation(actual, laberinto.getSalida());
            Celda cAnterior = actual;
            for (Celda c :
                    adyacentes) {
                if (c != null && !celdas.contains(c)) {
                    hActual = heuristic.estimation(c, laberinto.getSalida());
                    if (hActual <= hAnterior && c.getCoste() + costeHastaAhora <= laberinto.getUmbral()) {
                        actual = c;
                        hAnterior = hActual;
                    }
                }
            }
            if (actual.equals(cAnterior))
                noMejor = true;
            else
                movsHastaAhora++;

            anterior = nodo;

        }

        causaFallo = "";
        if (!solucion) {
            if (costeHastaAhora > laberinto.getUmbral())
                causaFallo += "Umbral sobrepasado";
            else if (noMejor)
                causaFallo += "No hay nodos mejores";
            else
                causaFallo = "FALLO DESCONOCIDO";
        }

        getMovimientos();
    }
}
