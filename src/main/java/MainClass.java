import algorithm.*;
import heuristic.*;
import io.ReadData;
import io.WriteResults;
import struct.Laberinto;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class MainClass {
    public static void main(String[] args) throws Exception {
        if (args.length > 0) {
            ReadData rd = new ReadData(new File(args[0]));
            WriteResults wr = new WriteResults();
            Laberinto l = rd.leerLaberinto();
            System.out.println(l.toString());
            System.out.println();
            System.out.println();

            Map<Algorithm, Long> algorMap = new HashMap<>();

            Heuristic[] heuristics = {new CosteAdyacentes(), new Euclediana(), new Manhattan(), new Tablero()};
            Algorithm[] algorithms = {new EscaladaSimple(), new EscaladaMaxPendiente(), new PrimeroMejor(), new AStar()};

            for (Algorithm a :
                    algorithms) {
                for (Heuristic h :
                        heuristics) {
                    long nTime = System.nanoTime();
                    a.setup(l);
                    a.runWithHeuristic(h);
                    a.runAlgor();
                    long nTime2 = System.nanoTime();
                    a.setTime((nTime2 - nTime) / 1000);
                    System.out.println(a.execOutput());
                    System.out.printf("Time(us): %d\n", (nTime2 - nTime) / 1000);
                    wr.writeData(a);
                }
            }

            wr.end();

        } else {
            throw new Exception("Ponga un nombre de fichero como argumento para el programa");
        }
        //new visual.IASIFrame().setVisible(true);
    }
}
