package struct;

public class Celda {
    private final int coste;
    private final int id;
    private Celda NORTE,SUR,ESTE,OESTE;

    /**
     * Constructor de Celda
     *
     * @param coste Coste de celda
     * @param id    ID de celda
     */
    public Celda(int coste, int id) {
        this.coste = coste;
        this.id = id;
    }

    public int getCoste() {
        return coste;
    }

    public Celda getNORTE() {
        return NORTE;
    }

    public void setNORTE(Celda NORTE) {
        this.NORTE = NORTE;
    }

    public Celda getSUR() {
        return SUR;
    }

    public void setSUR(Celda SUR) {
        this.SUR = SUR;
    }

    public Celda getESTE() {
        return ESTE;
    }

    public void setESTE(Celda ESTE) {
        this.ESTE = ESTE;
    }

    public Celda getOESTE() {
        return OESTE;
    }

    public void setOESTE(Celda OESTE) {
        this.OESTE = OESTE;
    }

    public Celda getNORESTE() {
        if (NORTE != null)
            return NORTE.getESTE();
        return null;
    }

    public Celda getNOROESTE() {
        if (NORTE != null)
            return NORTE.getOESTE();
        return null;
    }

    public Celda getSURESTE() {
        if (SUR != null)
            return SUR.getESTE();
        return null;
    }

    public Celda getSUROESTE() {
        if (SUR != null)
            return SUR.getOESTE();
        return null;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        String norte = "", este = "", sur = "", oeste = "";
        if (NORTE != null)
            norte = "\tNorte: " + Integer.toString(NORTE.getId()) + "\n";
        if (ESTE != null)
            este = "\tEste: " + Integer.toString(ESTE.getId()) + "\n";
        if (OESTE != null)
            oeste = "\tOeste: " + Integer.toString(OESTE.getId()) + "\n";
        if (SUR != null)
            sur = "\tSur: " + Integer.toString(SUR.getId()) + "\n";

        return "Celda:" + id + "\n" + "Coste: " + coste + "\n" + norte + este + sur + oeste;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Celda celda = (Celda) o;
        return id == celda.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
