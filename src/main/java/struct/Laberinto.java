package struct;

import heuristic.Euclediana;
import heuristic.Heuristic;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Laberinto {
    public static final int MAX_SIZE =10;
    private static Laberinto instance;
    private final Celda[][] laberinto;
    private final int umbral;
    private final List<Celda> salidas;
    private final Celda salida;

    /**
     * Constructor de laberinto
     *
     * @param umbral Umbral del laberinto
     * @param celdas Matriz de celdas
     */
    private Laberinto(int umbral, Celda[][] celdas) {
        this.umbral = umbral;
        this.laberinto=celdas;
        salidas = new LinkedList<>();
        buscarSalidas();
        salida = getSalidas().get(0);
    }

    public static void destroy() {
        instance = null;
    }

    public static Laberinto getInstance(int umbral, Celda[][] celdas) {
        if (instance == null)
            instance = new Laberinto(umbral, celdas);
        return instance;
    }

    private void buscarSalidas() {
        for (int i = 0; i < MAX_SIZE; i++) {
            for (int j = 0; j < MAX_SIZE; j++) {
                if (laberinto[i][j].getCoste() == 0) {
                    salidas.add(laberinto[i][j]);
                }
            }
        }
    }

    public List<Celda> getSalidas() {
        Heuristic euc = new Euclediana();
        salidas.sort(Comparator.comparingDouble(celda -> euc.estimation(laberinto[0][0], celda)));
        return salidas;
    }

    private void setCells(){}

    public Celda[][] getLaberinto() {
        return laberinto;
    }

    public int getUmbral() {
        return umbral;
    }

    @Override
    public String toString() {
        String lab = new String();
        for (int i = 0; i < MAX_SIZE; i++) {
            for (int j = 0; j < MAX_SIZE; j++) {
                int coste = laberinto[i][j].getCoste();
                if (coste != 0)
                    lab += "\t" + (coste + " ");
                else
                    lab += "\t" + ("\u001B[36m" + coste + "\u001B[0m" + " ");
            }
            lab += "\n";
        }
        return lab;
    }

    public Celda getSalida() {
        return salida;
    }

    public String colouredLaberint(Set<Celda>... celdas) {
        String lab = new String();
        for (int i = 0; i < MAX_SIZE; i++) {
            for (int j = 0; j < MAX_SIZE; j++) {
                int coste = laberinto[i][j].getCoste();
                if (celdas[0].contains(laberinto[i][j]))
                    lab += "\t" + ("\u001B[34m" + coste + "\u001B[0m" + " ");
                else {

                    if (celdas.length >= 2 && celdas[1].contains(laberinto[i][j]))
                        lab += "\t" + ("\u001B[32m" + coste + "\u001B[0m" + " ");
                    else if (celdas.length == 3 && celdas[2].contains(laberinto[i][j]))
                        lab += "\t" + ("\u001B[31m" + coste + "\u001B[0m" + " ");
                    else
                        lab += "\t" + (coste + " ");
                }
            }
            lab += "\n";
        }
        return lab;
    }
}
