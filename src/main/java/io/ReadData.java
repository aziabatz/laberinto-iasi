package io;

import struct.Celda;
import struct.Laberinto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReadData {
    private FileReader fr;
    private BufferedReader bufferedReader;

    /**
     * Constructor
     *
     * @param filename Nombre del fichero de configuración
     * @throws IOException En caso de fallo de lectura
     */
    public ReadData(File filename) throws IOException {
        fr = new FileReader(filename);
        bufferedReader = new BufferedReader(fr);
    }

    /**
     * Lee y crea un laberinto según la configuración del fichero
     * @return Devuelve una única instancia de Laberinto
     * @throws IOException
     */
    public Laberinto leerLaberinto() throws IOException{
        String line = bufferedReader.readLine();
        Laberinto laberinto;
        Celda[][] costes = new Celda[10][10];
        int umbral = Integer.parseInt(line);
        int id = 0;
        for (int i = 0; i < 10; i++) {
            String[] fila = bufferedReader.readLine().split(",");
            for (int j = 0; j < 10; j++) {
                costes[i][j] = new Celda(Integer.parseInt(fila[j]), id);
                id++;
            }
        }

        unirCeldas(costes);
        laberinto = Laberinto.getInstance(umbral, costes);

        return laberinto;
    }

    /**
     * Une las celdas para que se puedan referenciar directamente después
     * @param costes Matriz de celdas
     */
    private void unirCeldas(Celda[][] costes) {
        /*
        NORTE NULL FILA=0
        ESTE  NULL COLM=10
        OESTE NULL COLM=0
        SUR   NULL FILA=10
         */

        for (int fila = 0; fila < 10; fila++) {
            for (int columna = 0; columna < 10; columna++) {
                Celda c = costes[fila][columna];
                if (fila > 0)//norte
                    c.setNORTE(costes[fila - 1][columna]);
                if (columna < 9)//este
                    c.setESTE(costes[fila][columna + 1]);
                if (columna > 0)//oeste
                    c.setOESTE(costes[fila][columna - 1]);
                if (fila < 9)//sur
                    c.setSUR(costes[fila + 1][columna]);
            }
        }
    }
}
