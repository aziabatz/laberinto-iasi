package io;

import algorithm.Algorithm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class WriteResults {
    private FileWriter fw;
    private BufferedWriter bw;

    /**
     * Constructor
     *
     * @throws IOException En caso de fallo de escritura
     */
    public WriteResults() throws IOException {
        fw = new FileWriter("log.txt");
        bw = new BufferedWriter(fw);
    }

    /**
     * Escribe un log en función del algoritmo
     * @param a Algoritmo para representar en el log
     * @throws IOException En caso de fallo de escritura
     */
    public void writeData(Algorithm a) throws IOException {
        bw.write("=================================\n");
        bw.write(a.getNAME());
        bw.newLine();
        bw.write("Heuristica: " + a.getHeuristic().getClass().getSimpleName() + "\n");
        bw.write("Solución: " + a.isSolucion() + "\n");
        if (a.isSolucion()) {
            bw.write("Ruta: " + Arrays.toString(a.getMovimientos()) + "\n");
        } else {
            bw.write("Causa de fallo: " + a.getCausaFallo() + "\n");
        }
        bw.write("Tiempo empleado: " + a.getTime() + "\n");
        bw.write("Nodos generados: " + a.nodosSize() + "\n");
        bw.write("=================================\n");
        bw.flush();

    }

    /**
     * Vacía el flujo y lo cierra
     */
    public void end() {
        try {
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
